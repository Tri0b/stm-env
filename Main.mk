include Makefile

upload:
	openocd -f "scripts/board/st_nucleo_f4.cfg" -c "program build/$(TARGET).elf verify reset exit"
